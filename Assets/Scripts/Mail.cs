﻿using UnityEngine;
using System.Collections;

public class Mail : MonoBehaviour
{
	public Facteur facteur;
	public int movingDirection = 1;
	public float SPEED;
	private static float MIN_POSITION_X = -12f;
	private static float MAX_POSITION_X = 12f;
	public enum MailDirection
	{
		Left,
		Right,
		BothSide
	}

	// Use this for initialization
	void Start ()
	{
		SPEED = 0.2f;
	}

	void Update ()
	{
		transform.Rotate (Vector3.forward * -5);
	}

	void FixedUpdate ()
	{
		this.transform.position = new Vector2 (transform.position.x + SPEED * movingDirection, transform.position.y);
		if ((transform.position.x > MAX_POSITION_X) || (transform.position.x < MIN_POSITION_X)) {
			Destroy (this.gameObject);
			facteur.mailSent--;
			facteur.NotifyScoreToHUD (-10);
		}
	}

	void OnTriggerEnter2D (Collider2D coll)
	{
		if (coll.gameObject.tag == "mailbox") {
			facteur.NotifyScoreToHUD (50);
		} else if (coll.gameObject.tag == "chien") {
			facteur.NotifyScoreToHUD (-50);
		}
		Destroy (this.gameObject);
		facteur.mailSent--;
	}
}
