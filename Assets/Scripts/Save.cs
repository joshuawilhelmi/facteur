﻿using UnityEngine;
using System.Collections;

public class Save
{
	public enum Vitesse
	{
		NORMAL,
		RAPIDE,
		TRES_RAPIDE
	}


	public static int DureePartie {
		get { return (PlayerPrefs.GetInt ("duree_partie") == 0) ? 60 : PlayerPrefs.GetInt ("duree_partie"); }
		set { PlayerPrefs.SetInt ("duree_partie", value); }
	}

	public static Vitesse VitesseJeu {
		get { return (PlayerPrefs.GetString ("vitesse") == "") ? Vitesse.NORMAL : (Vitesse)Vitesse.Parse (typeof(Vitesse), PlayerPrefs.GetString ("vitesse")); }
		set { PlayerPrefs.SetString ("vitesse", value.ToString ());  }
	}

	public static string VitesseJeuToString ()
	{
		string vitesse = "Normal";
		switch (VitesseJeu) {
		case Vitesse.NORMAL:
			vitesse = "Normal";
			break;
		case Vitesse.RAPIDE:
			vitesse = "Rapide";
			break;
		case Vitesse.TRES_RAPIDE:
			vitesse = "Très rapide";
			break;
		}
		return vitesse;
	}

    

}
