﻿using UnityEngine;
using System.Collections;

public class Facteur : MonoBehaviour
{

	public GameObject HUD;
	private HUDGame hudGame;

	public int mailSent;

	private int delayForInput;
	private static int DELAY_TO_WAIT_INPUT = 15;

	private bool leftInput;
	private bool rightInput;

	public Decor decor;
	public GameObject DecorEnCours {
		get { return decor.decorToReUp; }
	}


	// Use this for initialization
	void Start ()
	{
		decor = GameObject.Find ("Decor").GetComponent<Decor> ();
		hudGame = HUD.GetComponentInChildren<HUDGame> ();
	}

	void FixedUpdate ()
	{

	}

	// Update is called once per frame
	void Update ()
	{
		if (hudGame.GameIsRunning) {
			if (mailSent == 0) {
				ProcessInput ();
				if (delayForInput++ >= DELAY_TO_WAIT_INPUT) {
					delayForInput = 0;
					if (leftInput && rightInput) {
						SendLetter (Mail.MailDirection.BothSide);
					} else if (leftInput) {
						SendLetter (Mail.MailDirection.Left);
					} else if (rightInput) {
						SendLetter (Mail.MailDirection.Right);
					}
					leftInput = false;
					rightInput = false;
				}
			}
		}
	}

	public void NotifyScoreToHUD (int score)
	{
		hudGame.score += score;
	}

	void ProcessInput ()
	{
        
		if (Input.GetKey (KeyCode.RightArrow)) {
			rightInput = true;
			//SendLetter (1);
		}
		if (Input.GetKey (KeyCode.LeftArrow)) {
			leftInput = true;
			//SendLetter (-1);
		}
	}

	void SendLetter (Mail.MailDirection direction)
	{
		if (direction == Mail.MailDirection.BothSide || direction == Mail.MailDirection.Left) {
			GameObject newMail = GameObject.Instantiate (Resources.Load ("mail")) as GameObject;

			Mail mailScript = newMail.GetComponent<Mail> ();
			mailScript.facteur = this;
			mailScript.movingDirection = -1;
			newMail.renderer.sortingOrder = 20;
			newMail.transform.position = new Vector2 (transform.position.x - 0.5f, transform.position.y);
			mailSent++;
		}

		if (direction == Mail.MailDirection.BothSide || direction == Mail.MailDirection.Right) {
			GameObject newMail = GameObject.Instantiate (Resources.Load ("mail")) as GameObject;

			Mail mailScript = newMail.GetComponent<Mail> ();
			mailScript.facteur = this;
			mailScript.movingDirection = 1;
			newMail.renderer.sortingOrder = 20;
			newMail.transform.position = new Vector2 (transform.position.x + 0.5f, transform.position.y);
			mailSent++;
		}
	}
}
