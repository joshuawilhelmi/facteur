﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDGame : MonoBehaviour
{

	private GameObject TimerInHUD;
	private GameObject ScoreInHUD;
	private GameObject ScoreFinal;

	private float timer;
	public int score;
	public bool GameIsRunning;

	// Use this for initialization
	void Start ()
	{
		ScoreInHUD = transform.FindChild ("TxtScore").GetChild (0).gameObject;
		TimerInHUD = transform.FindChild ("Timer").gameObject;
		ScoreFinal = transform.FindChild ("EndOfGame").FindChild ("ScoreFinal").gameObject;
		timer = Save.DureePartie;
		GameIsRunning = true;
	}

	void FixedUpdate ()
	{
		MajTimer ();
		MajScore ();
	}

	void MajScore ()
	{
		ScoreInHUD.GetComponent<Text> ().text = score.ToString ();
	}

	void MajTimer ()
	{
		timer -= Time.deltaTime;
		TimerInHUD.GetComponent<Text> ().text = Mathf.Round (timer).ToString ();
		if (Mathf.Round (timer) == 0f) {
			GameIsRunning = false;
			EndOfGame ();
		}
	}

	// Update is called once per frame
	void Update ()
	{
	
	}

	void EndOfGame ()
	{
		ScoreFinal.GetComponent<Text> ().text = "Votre score final est de : " + score.ToString () + " points";
		transform.FindChild ("EndOfGame").gameObject.SetActive (true);
	}

	public void RetourMenu ()
	{
		Application.LoadLevel ("Menu");
	}
}
