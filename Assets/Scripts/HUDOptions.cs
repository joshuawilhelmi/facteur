﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDOptions : MonoBehaviour
{
	public GameObject txtDifficulte;
	public GameObject txtDuree;

	void Start ()
	{
		MajTexteDuree ();
		MajTexteDifficulte ();
	}

	private void MajTexteDifficulte ()
	{
		txtDifficulte.GetComponent<Text> ().text = "Vitesse : " + Save.VitesseJeuToString ();
	}


	private void MajTexteDuree ()
	{
		txtDuree.GetComponent<Text> ().text = "Durée de partie : " + Save.DureePartie + " secondes";
	}
	public void ChooseFacile ()
	{
		Save.VitesseJeu = Save.Vitesse.NORMAL;
		MajTexteDifficulte ();
	}
	public void ChooseMoyen ()
	{
		Save.VitesseJeu = Save.Vitesse.RAPIDE;
		MajTexteDifficulte ();
	}
	public void ChooseDifficile ()
	{

		Save.VitesseJeu = Save.Vitesse.TRES_RAPIDE;
		MajTexteDifficulte ();
	}

	public void Choose30Secondes ()
	{
		Save.DureePartie = 30;
		MajTexteDuree ();
	}
	public void Choose60Secondes ()
	{
		Save.DureePartie = 60;
		MajTexteDuree ();
	}
	public void Choose90Secondes ()
	{
		Save.DureePartie = 90;
		MajTexteDuree ();
	}

	public void Retour ()
	{
		Application.LoadLevel ("Menu");
	}
}
