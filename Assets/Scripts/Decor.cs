﻿using UnityEngine;
using System.Collections;

public class Decor : MonoBehaviour
{

	private HUDGame hudGame;
	private static float POSITION_Y_FIRES_REUP = -16f;
	private static float Y_TO_REUP = 28.5f;
	private static float SPEED_SCROLL;

	public GameObject decor1;
	public GameObject decor2;

	// Use this for initialization
	void Start ()
	{
		hudGame = GameObject.Find ("HUD").GetComponent<HUDGame> ();
		switch (Save.VitesseJeu) {
		case Save.Vitesse.NORMAL:
			SPEED_SCROLL = .05f;
			break;
		case Save.Vitesse.RAPIDE:
			SPEED_SCROLL = .1f;
			break;
		case Save.Vitesse.TRES_RAPIDE:
			SPEED_SCROLL = .15f;
			break;
		}
	}

	public GameObject decorToReUp {
		get {
			if (decor1.transform.position.y < decor2.transform.position.y) {
				return decor1;
			}
			return decor2;
		}
	}

	public GameObject otherDecor {
		get {
			if (decorToReUp == decor1) {
				return decor2;
			}
			return decor1;
		}
	}




	void FixedUpdate ()
	{
		if (hudGame.GameIsRunning) {
			decor1.transform.position = new Vector2 (decor1.transform.position.x, decor1.transform.position.y - SPEED_SCROLL);
			decor2.transform.position = new Vector2 (decor2.transform.position.x, decor2.transform.position.y - SPEED_SCROLL);

			if (decorToReUp.transform.position.y < POSITION_Y_FIRES_REUP) {
				decorToReUp.transform.position = new Vector2 (decorToReUp.transform.position.x, decorToReUp.transform.position.y + Y_TO_REUP);
			}
		}
	}


	// Update is called once per frame
	void Update ()
	{
		
	}
}
